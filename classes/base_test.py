from selenium.webdriver.remote.webdriver import WebDriver

from classes.page_objects.page import Page


class BaseTest:
    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.driver.implicitly_wait(10)
        self.page = Page(self.driver)

    def open(self, url: str):
        return self.driver.get(url)



