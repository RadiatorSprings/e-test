from time import sleep

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement


class Component:
    def __init__(self, driver: WebDriver, element: WebElement):
        self._driver = driver
        self._element = element

    def click(self):
        return self._element.click()

    @property
    def text(self):
        return self._element.text

    def wait_for_visible(self):
        for i in range(10):
            if self._element.is_displayed():
                return True
            sleep(0.5)
        return False

    def wait_for_invisible(self):
        for i in range(10):
            if not self._element.is_displayed():
                return True
            sleep(0.5)
        return False

    def wait_text(self, text: str):
        for i in range(10):
            if self._element.text.lower() == text.lower():
                return True
            sleep(0.5)
        return False

    @property
    def is_exist(self):
        return bool(self._element)
