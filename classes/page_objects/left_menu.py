from classes.find_elements import find_element
from classes.page_objects.Component import Component


class LeftMenu(Component):
    LEFT_MENU_CONTAINER = '//*[@id="root"]/div[2]/main/div/div[1]'
    MY_ACCOUNTS_ITEM = '//*[@id="root"]/div[2]/main/div/div[1]/div[1]/a[1]'
    DEPOSIT_ITEM = '//*[@id="root"]/div[2]/main/div/div[1]/div[1]/a[2]'
    WITHDRAWAL_ITEM = '//*[@id="root"]/div[2]/main/div/div[1]/div[1]/a[3]'

    @property
    def my_accounts(self):
        return Component(self._driver, find_element(self._driver, self.MY_ACCOUNTS_ITEM))

    @property
    def deposit(self):
        return Component(self._driver, find_element(self._driver, self.DEPOSIT_ITEM))

    @property
    def withdrawal(self):
        return Component(self._driver, find_element(self._driver, self.WITHDRAWAL_ITEM))
