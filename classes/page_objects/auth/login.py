from classes.find_elements import find_element, find_element_by_id
from classes.page_objects.Component import Component


class Login(Component):
    FORM_XPATH = '//signin-form/form'
    SUBMIT_BUTTON_XPATH = '//exwc-button/button'
    LOGIN_ID = 'login'
    PASSWORD_ID = 'password'

    def set_login(self, login):
        element = find_element_by_id(self._driver, self.LOGIN_ID)
        element.send_keys(login)
        return self

    def set_password(self, password):
        element = find_element_by_id(self._driver, self.PASSWORD_ID)
        element.send_keys(password)
        return self

    @property
    def submit(self):
        return find_element(self._driver, self.SUBMIT_BUTTON_XPATH)
