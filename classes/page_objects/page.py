from classes.find_elements import find_element
from classes.page_objects.auth.login import Login
from classes.page_objects.left_menu import LeftMenu
from classes.page_objects.my_accounts.my_accounts import MyAccounts


class Page:
    def __init__(self, driver):
        self.__driver = driver

    @property
    def sign_in(self):
        return Login(self.__driver, find_element(self.__driver, Login.FORM_XPATH))

    @property
    def my_accounts(self):
        return MyAccounts(self.__driver, find_element(self.__driver, MyAccounts.ACCOUNTS_FRAME_XPATH))

    @property
    def left_menu(self):
        return LeftMenu(self.__driver, find_element(self.__driver, LeftMenu.LEFT_MENU_CONTAINER))
