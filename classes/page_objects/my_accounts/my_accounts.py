from classes.find_elements import find_element
from classes.page_objects.Component import Component
from classes.page_objects.my_accounts.archived import Archived
from classes.page_objects.my_accounts.demo import Demo


class MyAccounts(Component):
    ACCOUNTS_FRAME_XPATH = '//*[@id="root"]/div[2]/main/div/div[2]/div[1]/div/div'

    @property
    def tab_demo(self):
        return Demo(self._driver, find_element(self._driver, Demo.TAB_DEMO_XPATH))

    @property
    def tab_archived(self):
        return Archived(self._driver, find_element(self._driver, Archived.ARCHIVED_TAB))

    @property
    def tab_real(self):
        pass

