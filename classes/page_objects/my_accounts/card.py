from classes.find_elements import find_element
from classes.page_objects.Component import Component


class Card(Component):
    CARD_CONTAINER = '//*[@id="root"]/div[2]/main/div/div[2]/div[1]/div/div/div[4]/div[1]/div[1]'
    COG = '//*[@id="root"]/div[2]/main/div/div[2]/div[1]/div/div/div[4]/div[1]/div[1]/div/div[2]/div[1]/div[2]/div'
    ARCHIVED_LINK = '//*[@id="root"]/div[2]/main/div/div[2]/div[1]/div/div/div[4]/div[1]/div[1]/div/div[2]/div[1]/div[2]/div/div[2]/div/ul/li[6]'
    CARD_ID = '//*[@id="root"]/div[2]/main/div/div[2]/div[1]/div/div/div[4]/div[1]/div[1]/div/div[1]/div[4]/div'
    CARD_ID_FOR_SEARCH = '//*[@id="root"]/div[2]/main/div/div[2]/div[1]/div/div/div[4]/div[1]/div[1]/div/div[1]/div[4]/div[Text() = "%s"]'
    REACTIVATE_BUTTON = '//*[@id="root"]/div/main/div/div/div/div/div/div/div/div/div/div/div/div/div/button'

    def cog_archived(self):
        find_element(self._driver, self.COG).click()
        find_element(self._driver, self.ARCHIVED_LINK).click()

    def card_id(self):
        return find_element(self._driver, self.CARD_ID).text

    def reactivate(self):
        reactivate = Component(self._driver, find_element(self._driver, self.REACTIVATE_BUTTON))
        reactivate.wait_text('Reactivate')
        reactivate.click()
