from classes.find_elements import find_element
from classes.page_objects.Component import Component
from classes.page_objects.my_accounts.card import Card


class Archived(Component):
    ARCHIVED_TAB = '//*[@id="root"]/div[2]/main/div/div[2]/div[1]/div/div/div[2]/div[text() = "Archived"]'

    @property
    def card(self):
        return Card(self._driver, find_element(self._driver, Card.CARD_CONTAINER))


