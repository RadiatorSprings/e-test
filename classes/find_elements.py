from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webdriver import WebDriver


def find_element(driver: WebDriver, xpath: str):
    try:
        elem = driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        elem = None

    return elem


def find_element_by_id(driver: WebDriver, id: str):
    try:
        elem = driver.find_element_by_id(id)
    except NoSuchElementException:
        elem = None

    return elem
