import os

from config.config import Selenium, Base, User


class Cfg(Base, User, Selenium):
    def fetch(self):
        return {
            "SELENIUM_HOST": os.getenv("SELENIUM_HOST", self.host),
            "SELENIUM_PORT": os.getenv("SELENIUM_PORT", self.port),
            "USER_LOGIN": os.getenv("USER_LOGIN", self.login),
            "USER_PASSWORD": os.getenv("SELENIUM_PORT", self.password),
            "BASE_URL": os.getenv("SELENIUM_PORT", self.url),
            "BROWSER_NAME": os.getenv("BROWSER_NAME", self.browser_name),
            "BROWSER_VERSION": os.getenv("BROWSER_VERSION", self.browser_version),
        }
