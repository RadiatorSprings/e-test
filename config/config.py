from dataclasses import dataclass


@dataclass
class Base:
    url = 'https://my.exness.com'


@dataclass
class User:
    login = "m.kuznetsov@mail.ru"
    password = "123QWEqwe"


@dataclass
class Selenium:
    host: str = "192.168.0.102"
    port: int = 4444
    browser_name: str = "chrome"
    browser_version: str = ""  # запускается на любой доступной
