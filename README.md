# e-test

Как запустить локально?

1. Запустить selenium командой `docker-compose up -d`
2. Создать виртуальное окружение и активировать его
3. Установить зависимости `pip3 install -r requirements.txt`
4. Запустить тесты `pytest`
    - Для получения файла отчета `pytest --html=./reports/report.html`
    - Чтобы запустить тесты в паралель `pytest -n 2`

Как запустить с помощью докера?
1. Запустить selenium командой `docker-compose up -d`
2. Запустить команду `make run_test`
