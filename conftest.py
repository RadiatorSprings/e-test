import datetime

import pytest
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.remote import webdriver

from classes.base_test import BaseTest
from config.env import Cfg


@pytest.fixture(scope="class")
def ctx():
    cfg = Cfg().fetch()
    capability = {
        "browserName": cfg['BROWSER_NAME'],
        "version": cfg['BROWSER_VERSION'],
        "platform": "ANY"
    }
    command_executor = 'http://{}:{}/wd/hub'.format(cfg['SELENIUM_HOST'], cfg['SELENIUM_PORT'])
    driver = webdriver.WebDriver(command_executor=command_executor, desired_capabilities=capability)
    yield BaseTest(driver)
    driver.close()


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def make_screenshot(item, call):
    outcome = yield
    rep = outcome.get_result()
    if rep.failed:
        item.funcargs['ctx']\
            .driver\
            .save_screenshot("./screenshots/{}::{}::{}.png".format(
                datetime.datetime.now(),
                rep.fspath,
                rep.head_line
            ))
