FROM python:3.7-slim

RUN mkdir ./exness
COPY ./requirements.txt ./exness/requirements.txt

RUN pip3 install -r ./exness/requirements.txt

COPY ./ ./exness

WORKDIR ./exness