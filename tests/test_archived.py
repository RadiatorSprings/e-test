from time import sleep

import pytest

from config.env import Cfg


class TestClass:
    @pytest.fixture(autouse=True)
    def parameters(self):
        cfg = Cfg().fetch()
        self.url = cfg["BASE_URL"]
        self.user = cfg["USER_LOGIN"]
        self.password = cfg["USER_PASSWORD"]

    def test_archive(self, ctx):
        ctx.open(self.url)
        ctx.page.sign_in.set_login(self.user).set_password(self.password).submit.click()
        assert ctx.page.left_menu.my_accounts.wait_for_visible(), "Не отобразился пункт меню 'My accounts'"
        ctx.page.left_menu.my_accounts.click()
        assert ctx.page.my_accounts.wait_for_visible(), "Не загрузили страницу 'My accounts'"
        ctx.page.my_accounts.tab_demo.click()
        assert ctx.page.my_accounts.tab_demo.card.wait_for_visible(), "Не открылись аккаунты на Demo странице"
        card_id = ctx.page.my_accounts.tab_demo.card.card_id()
        ctx.page.my_accounts.tab_demo.card.cog_archived()
        assert ctx.page.my_accounts.tab_demo.card_is_exist(card_id) is False, "Аккаунт не переместился в раздел 'Archived'"
        assert ctx.page.my_accounts.tab_archived.wait_for_visible(), "После архивирования не появился таб 'Archived'"
        ctx.page.my_accounts.tab_archived.click()
        archived_card_id = ctx.page.my_accounts.tab_demo.card.card_id()
        assert card_id == archived_card_id, "Заархивировался неверный аккаунт"
        ctx.page.my_accounts.tab_archived.card.reactivate()
        assert ctx.page.my_accounts.tab_archived.is_exist is False, "После реактивации аккаунта не исчез таб 'Archived'"
